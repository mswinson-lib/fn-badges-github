## Prerequisites

    docker-app
    docker-compsoe

## Running openfaas locally

    docker app install mswinson/openfaas-swarm --name openfaas

## Building functions 

    export DOCKER_REPO=<myrepo>
    export DOCKER_USER=<myuser>
    export DOCKER_PASS=<mypassword>

    docker-compose run sandbox

    make build   # build function container
    make smoke   # run smoke test
    make release # release to dockerhub

