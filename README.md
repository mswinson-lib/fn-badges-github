# fn-badges-github

    faas to return badges from a github repository

## Description

    given a github repository url, returns a json file containing a list of the
    badges contained in the repository's README.md file.

## Prerequisites

    faas-cli
    a running openfaas stack

## Usage

    faas-cli store -u https://bitbucket.org/mswinson-lib/fn-store/raw/master/store.json deploy fn-badges-github
    faas-cli invoke fn-badges-github --query url=https://github.com/openfaas/faas

    
