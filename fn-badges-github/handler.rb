require 'uri'
require 'open-uri'
require 'json'

BADGE_REGEX = /\[!\[([A-Za-z ]*)\]\(([^)]*)\)\]\(([^)]*)\)/

VALID_URIS = [
  {
    'scheme' => /^http$/,
    'hostname' => /^github\.com$/,
    'path' => /\/[^\/]+\/.*/
  },
  {
    'scheme' => /^https$/,
    'hostname' => /^github\.com$/,
    'path' => /\/[^\/]+\/.*/
  }
]

class Handler

  # fetch badges
  # @api public
  # @return [String]
  def run(req)
    response = run_request(req)
    JSON.pretty_generate(response)
  end

  private

  # run request
  # @api private
  # @param req [Request] request
  # @return [Hash]
  def run_request(req)
    query_params = query_params_for(ENV['Http_Query'])
    url = query_params['url'] || ''
    uri = URI(url)
    validate_uri(uri)
    repo = uri.path.gsub(/^\//,'')

    badges = badges_from_readme(repo, 'master')
    {
      'result' => 'success',
      'body' => badges
    }
  rescue ArgumentError
    {
      'result' => 'error',
      'body' => {
        'message'=> 'invalid uri',
        'params' => query_params
      }
    }
  end

  # url is valid?
  # @api private
  # @param uri [URI] uri
  # @return void
  # @raise ArgumentError - if not an accepted uri
  def validate_uri(uri)
    VALID_URIS.each do |uri_spec|
      next unless uri_spec['scheme'] =~ uri.scheme
      next unless uri_spec['hostname'] =~ uri.hostname
      next unless uri_spec['path'] =~ uri.path

      return
    end
    raise ArgumentError
  end

  # parse out query string 
  # @api private
  # @param query_string [String] query string
  # @return [Hash]
  def query_params_for(query_string)
    query_params = query_string.split(';')
    Hash[query_params.map { |param| param.split('=') }]
  end

  # badge data from readme file
  # @api private
  # @param repo [String] github rpeo
  # @param branch [String] github branch
  # @return [Hash]
  # @example - valid repo
  #   badges_from_readme('openfaas/faas', 'master')
  #   => {
  #     'repo' => 'openfaas/faas',
  #     'badges' => [
  #       {
  #         'title' => '..',
  #         'image_url' => '..',
  #         'link_url' => '..'
  #       }
  #     ]
  #   }
  def badges_from_readme(repo, branch)
    readme_source = fetch_readme(repo, branch)

    matches = readme_source.scan(BADGE_REGEX)
    badges = {
      "repo" => repo,
      "badges" => []
    }

    matches.each do |match|
      title = match[0]
      image_url = match[1]
      link_url = match[2]

      badges["badges"] << {
        "title" => title,
        "image_url" => image_url,
        "link_url" => link_url
      }
    end
    badges
  end

  # fetch readme file
  # @api private
  # @param repo [String] github repo
  # @param branch [String] github branch
  # @return [String]
  # @example  - valid repo
  #   fetch_readme('openfaas/faas', 'master')
  #   => '.....'
  # @example  - invalid repo
  #   fetch_readme('abc/def', 'master')
  #   => ''
  def fetch_readme(repo, branch)
    repo_url = "https://raw.githubusercontent.com/#{repo}/#{branch}/README.md"
    url = URI(repo_url)
    source = url.read
    source
  rescue OpenURI::HTTPError
    ""
  end
end
