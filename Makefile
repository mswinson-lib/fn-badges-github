.PHONY: init build deploy release clean
.EXPORT_ALL_VARIABLES:

DOCKER_REPO ?= $(error "DOCKER_REPO not defined")
DOCKER_USER ?= $(error "DOCKER_USER not defined")
DOCKER_PASS ?= $(error "DOCKER_PASS not defined")

PROJECTNAME ?= fn-badges-github
PROJECT_LANG ?= ruby
VERSION=0.1
BRANCH ?= develop

TAG ?= $(BRANCH)-$(VERSION)

DOCKER_ARGS=--no-cache

OPENFAAS_GW_URL ?= http://127.0.0.1:8080

build:
	@faas-cli build -f $(PROJECTNAME).yml $(DOCKER_ARGS)

release:
	@docker login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@faas-cli push -f $(PROJECTNAME).yml

clean:
	@rm -rf build

smoke:
	@faas-cli deploy --gateway $(OPENFAAS_GW_URL) -f $(PROJECTNAME).yml
	@echo '' | faas-cli invoke $(PROJECTNAME) --gateway $(OPENFAAS_GW_URL) --query url=http://github.com/openfaas/faas
	@echo '' | faas-cli invoke $(PROJECTNAME) --gateway $(OPENFAAS_GW_URL) --query url=https://github.com/openfaas/faas

